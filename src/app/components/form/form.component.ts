import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { dateValidator } from '../../validators/date-validator';
import { RestService } from '../../services/http.service';
import {TableService} from '../../services/table.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormComponent  {
  public form: FormGroup ;

  constructor(
    private fb: FormBuilder,
    private restService: RestService,
    private tableService: TableService
  ) {
    this.form = this.fb.group({
      symbol: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(4)]],
      start_date: [null, Validators.required],
      end_date: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]]
    }, { validators: dateValidator });
  }

  startDateChange(startDate: any): void {
    this.form.controls.start_date.setValue(startDate);
  }

  endDateChange(endDate: any): void {
    this.form.controls.end_date.setValue(endDate);
  }

  submitForm(): void {
    if (this.form.valid) {
      this.restService.post('/api/charts', this.form.value).subscribe((tableData: any[]) => {
        this.tableService.setTableData(tableData);
        this.tableService.setTableVisibility(Boolean(tableData?.length));
      });
    }
  }
}
