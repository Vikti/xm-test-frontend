import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import { TableService } from '../../services/table.service';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import {UnsubscribeDirective} from '../../utils/unsubscribe';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartComponent extends UnsubscribeDirective implements OnInit {
  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Open'},
    { data: [50, 30, 10, 60, 80, 90, 40], label: 'Close'},
  ];
  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];

  constructor(
    private tableService: TableService,
    private cd: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit(): void {
    this.tableService.getTableData().pipe(takeUntil(this.destroy$)).subscribe(tableData => {
      this.lineChartData = tableData.reduce((acc, row) => {
        acc[0].data.push(row.open);
        acc[1].data.push(row.close);

        return acc;
      }, [{ data: [], label: 'Open'}, { data: [], label: 'Close'}]);

      this.lineChartLabels = tableData.map(row => row.date);
      this.cd.markForCheck();
    });
  }

}
