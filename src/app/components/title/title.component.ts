import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TitleComponent {

}
