import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {TableService} from '../../services/table.service';
import {takeUntil} from 'rxjs/operators';
import {UnsubscribeDirective} from '../../utils/unsubscribe';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableComponent extends UnsubscribeDirective implements OnInit {
  tableHeaders = ['Date', 'Open', 'High', 'Low', 'Close', 'Volume'];
  tableData: any[] = [];

  constructor(
    private tableService: TableService,
    private cd: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit(): void {
    this.tableService.getTableData().pipe(takeUntil(this.destroy$)).subscribe(tableData => {
      this.tableData = tableData;
      this.cd.markForCheck();
    });
  }

  public trackByFn(index: number, item: any): any {
    return index;
  }
}
