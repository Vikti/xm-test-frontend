import {AfterViewInit, Directive, ElementRef, EventEmitter, NgZone, Output} from '@angular/core';

declare var $: any;

@Directive({
  selector: '[appDatepicker]'
})
export class DatepickerDirective implements AfterViewInit {
  @Output() dateChange = new EventEmitter();

  constructor(
    private el: ElementRef,
    private ngZone: NgZone
  ) { }

  ngAfterViewInit(): void {
    this.ngZone.runOutsideAngular(() => {
      $(this.el.nativeElement).datepicker({
        dateFormat : 'yy-mm-dd',
        onSelect: (date: any) => {
          this.ngZone.run(() => this.setDate(date));
        }
      });
    });
  }

  setDate(date: any): void {
    this.dateChange.emit(date);
  }
}
