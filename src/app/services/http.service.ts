import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  public baseUrl = 'http://localhost:8080';
  public CONTENT_TYPE_JSON = 'application/json';

  constructor(private http: HttpClient) { }

  public get(url: string, contentType: string = this.CONTENT_TYPE_JSON): Observable<any> {
    return this.http.get<any[]>(
      this.baseUrl + url,
      { headers: this.composeHeaders(contentType) }
    )
      .pipe(
        catchError(this.handleError(`get${url}`, []))
      );
  }

  public post(url: string, body: any, contentType: string = this.CONTENT_TYPE_JSON): Observable<any> {
    if (contentType === this.CONTENT_TYPE_JSON) {
      body = JSON.stringify(body);
    }

    return this.http
      .post(
        this.baseUrl + url,
        body,
        { headers: this.composeHeaders(contentType), responseType: 'json' })
      .pipe(
        catchError(this.handleError(`get${url}`, [])),
      );
  }

  private handleError<T>(operation = 'operation', result?: T): any{
    return (error: any): Observable<T> => {
      console.error(error);

      return of(result as T);
    };
  }

  private composeHeaders(type: string): HttpHeaders {
    return new HttpHeaders()
      .set('Content-Type', type)
      .set('Accept', 'application/json');
  }
}
