import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TableService {
  tableData = new BehaviorSubject<any[]>([{date: '-', open: '-', hi: '-', low: '-', close: '-', volume: '-'}]);
  isTableVisible = new BehaviorSubject<boolean>(false);

  constructor() { }

  setTableData(tableData: any[]): void {
    this.tableData.next(tableData);
  }

  getTableData(): Observable<any[]> {
    return this.tableData.asObservable();
  }

  setTableVisibility(isVisible: boolean): void {
    this.isTableVisible.next(isVisible);
  }

  getTableVisibility(): Observable<boolean> {
    return this.isTableVisible.asObservable();
  }
}
