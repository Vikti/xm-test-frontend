import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import { UnsubscribeDirective } from './utils/unsubscribe';
import {TableService} from './services/table.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent extends UnsubscribeDirective implements OnInit {
  isTableVisible = false;

  constructor(private tableService: TableService) {
    super();
  }

  ngOnInit(): void {
    this.tableService.getTableVisibility().pipe(takeUntil(this.destroy$)).subscribe(isVisible => this.isTableVisible = isVisible);
  }
}
