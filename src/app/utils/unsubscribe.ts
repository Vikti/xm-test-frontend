import { Directive, OnDestroy } from '@angular/core';

import { Subject } from 'rxjs';

@Directive({})
export abstract class UnsubscribeDirective implements OnDestroy {

    destroy$: Subject<boolean> = new Subject();

    ngOnDestroy(): void {
        this.destroy$.next(true);
        this.destroy$.complete();
    }
}
