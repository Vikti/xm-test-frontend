import { AbstractControl } from '@angular/forms';

export function dateValidator(control: AbstractControl): { [p: string]: boolean } | null {
  const startDateControl = control.get('start_date');
  const endDateControl = control.get('end_date');
  const startDate = startDateControl?.value;
  const endDate = endDateControl?.value;
  const startDateMs = +new Date(startDate);
  const endDateMs = +new Date(endDate);
  const currentDateMs = Date.now();

  if (startDate && !endDate && startDateMs > currentDateMs) {
    startDateControl?.setErrors({ dateInvalid: true });

    return { dateInvalid: true };
  } else if (!startDate && endDate && endDateMs > currentDateMs) {
    endDateControl?.setErrors({ dateInvalid: true });

    return { dateInvalid: true };
  } else if (startDate && endDate && (startDateMs > currentDateMs || endDateMs > currentDateMs || startDateMs > endDateMs)) {
    startDateControl?.setErrors({ dateInvalid: true });
    endDateControl?.setErrors({ dateInvalid: true });

    return  { dateInvalid: true };
  } else {
    startDateControl?.setErrors(null);
    endDateControl?.setErrors(null);

    return null;
  }
}
